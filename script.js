const bouton_previous = document.getElementById('previous')
const bouton_next = document.getElementById('next')
const image = document.getElementById('image')

bouton_previous.addEventListener('click', cacher)
bouton_next.addEventListener('click', cacher)

function cacher(){
    image.classList.toggle('image2')
}
